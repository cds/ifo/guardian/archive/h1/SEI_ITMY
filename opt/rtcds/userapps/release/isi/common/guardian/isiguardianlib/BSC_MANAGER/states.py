# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$

from guardian import GuardState
from guardian import NodeManager

from .. import const as top_const
from ..manager import *

##################################################

sub_names = dict(
    HPI = 'HPI_' + top_const.CHAMBER,
    ST1 = 'ISI_' + top_const.CHAMBER + '_ST1',
    ST2 = 'ISI_' + top_const.CHAMBER + '_ST2',)
nodes = NodeManager(sub_names.values())

##################################################

class INIT(GuardState):
    request = True

    @watchdog_dackill_check
    def main(self):
        log("initializing subordinate nodes...")

        nodes.set_managed()

        if watchdog_tripped_nodes(nodes):
            return 'WATCHDOG_TRIPPED_SUBORDINATE'

        # check for subordinate states:
        self.wait_for_isi_masterswitch = False
        self.wait_for_isi_coilmon = False

        if not coilmon_ok():
            # case when HPI masterswitch is on, but ISI coilmon not ok,
            log("ISI coilmon not ok...")
            #self.wait_for_isi_coilmon = True
            # Taking these out for now so we dont trip the chamber with a bad coil
            #nodes[sub_names['HPI']] = ISOLATED_STATE['HPI']
            #nodes[sub_names['ST1']] = 'READY'
            #nodes[sub_names['ST2']] = 'READY'
            #return 'ISI_COILMON_TRIPPED'

        if [node for node in nodes if node.state == 'INIT']:
            log("nodes in INIT; waiting for resolution...")
            self.return_state = 'INIT'

        elif not is_hpi_masterswitch_on():
            log("HPI masterswitch disengaged...")
            nodes[sub_names['HPI']] = 'READY'
            if not is_isi_masterswitch_on():
                nodes[sub_names['ST1']] = 'READY'
                nodes[sub_names['ST2']] = 'READY'
                self.return_state = 'OFFLINE'
            else:
                nodes[sub_names['ST1']] = 'DAMPED'
                nodes[sub_names['ST2']] = 'DAMPED'
                masterswitches_on(nodes)
                self.return_state = 'TURNING_ON_HPI_ISOLATION_LOOPS'

        elif not is_isi_masterswitch_on():
            # case when HPI masterswitch is on, but ISI switch is off,
            # which should only have happened due to manual
            # intervention.  set all requests corresponding to the
            # DAMPED state, then wait for the masterswitches to be
            # re-engaged
            log("ISI masterswitch disengaged...")
            self.wait_for_isi_masterswitch = True
            #nodes[sub_names['HPI']] = ISOLATED_STATE['HPI']
            nodes[sub_names['ST1']] = 'DAMPED'
            nodes[sub_names['ST2']] = 'DAMPED'
            self.return_state = 'DAMPED'

        elif nodes[sub_names['HPI']] == ISOLATED_STATE['HPI']:
            log("HPI is ISOLATED...")
            nodes[sub_names['HPI']] = ISOLATED_STATE['HPI']

            if nodes[sub_names['ST1']] == ISOLATED_STATE['ST1']:
                log("ST1 is ISOLATED...")
                nodes[sub_names['ST1']] = ISOLATED_STATE['ST1']

                if nodes[sub_names['ST2']] == ISOLATED_STATE['ST2']:
                    log("ST2 is ISOLATED...")
                    nodes[sub_names['ST2']] = ISOLATED_STATE['ST2']
                    self.return_state = 'FULLY_ISOLATED'
                else:
                    log("ST2 is DAMPED...")
                    nodes[sub_names['ST2']] = 'DAMPED'
                    self.return_state = 'ISOLATED_DAMPED'

            else:
                log("ST1/2 not ISOLATED...")
                log("resetting ST1/2 to DAMPED...")
                nodes[sub_names['ST1']] = 'DAMPED'
                nodes[sub_names['ST2']] = 'DAMPED'
                self.return_state = 'DAMPED'

        elif nodes[sub_names['HPI']] == 'READY':
            log("HPI is READY...")
            log("resetting everything to ready...")
            nodes[sub_names['HPI']] = 'READY'
            nodes[sub_names['ST1']] = 'READY'
            nodes[sub_names['ST2']] = 'READY'
            self.return_state = 'READY'

        else:
            log("unknown configuration (HPI = '%s'); resetting..." % nodes[sub_names['HPI']].state)
            nodes[sub_names['HPI']] = ISOLATED_STATE['HPI']
            nodes[sub_names['ST1']] = 'DAMPED'
            nodes[sub_names['ST2']] = 'DAMPED'
            self.return_state = 'DAMPED'

        log("next state: %s" % self.return_state)

    @get_subordinate_watchdog_check_decorator(nodes)
    @watchdog_dackill_check
    @nodes.checker()
    def run(self):
        if self.return_state == 'INIT':
            nodes_in_init = [node.name for node in nodes if node.state == 'INIT']
            if nodes_in_init:
                log("waiting for nodes to resolve INIT: %s" % nodes_in_init)
                return
            else:
                return 'INIT'

        if self.wait_for_isi_masterswitch:
            if not is_isi_masterswitch_on():
                notify("waiting for ISI masterswitch to be engaged...")
                return

        #if self.wait_for_isi_coilmon:
        #    if not coilmon_ok():
        #        notify("ISI coilmon not ok...")
        #        return
            
        for stalled_node in nodes.get_stalled_nodes():
            log("%s is stalled (request=%s, state=%s)" % (stalled_node.name, stalled_node.request, stalled_node.state))
            stalled_node.revive()

        if nodes.arrived:
            return self.return_state


WATCHDOG_TRIPPED_SUBORDINATE = get_watchdog_tripped_subordinate_state(nodes)

OFFLINE = get_offline_state(nodes)
READY = get_ready_state(nodes)

TURNING_ON_ST1_DAMPING_LOOPS = get_move_control_loops_state(nodes, sub_names['ST1'], 'DAMPED')
TURNING_ON_ST2_DAMPING_LOOPS = get_move_control_loops_state(nodes, sub_names['ST2'], 'DAMPED')

ISI_DAMPED_HEPI_OFFLINE = get_idle_state(nodes, requestable=True)
ISI_DAMPED_HEPI_OFFLINE.index = 35

TURNING_ON_HPI_ISOLATION_LOOPS = get_move_control_loops_state(nodes, sub_names['HPI'], ISOLATED_STATE['HPI'])

##########
TURN_ON_ST1_DAMPING_HEPI_UP = get_move_control_loops_state(nodes, sub_names['ST1'], 'DAMPED')
TURN_ON_ST2_DAMPING_HEPI_UP = get_move_control_loops_state(nodes, sub_names['ST2'], 'DAMPED')

TURN_OFF_ST2_DAMPING_HEPI_UP = get_move_control_loops_state(nodes, sub_names['ST2'], 'READY')
TURN_OFF_ST1_DAMPING_HEPI_UP = get_move_control_loops_state(nodes, sub_names['ST1'], 'READY')

ISI_OFFLINE_HEPI_ON = get_idle_state(nodes, requestable=True)
ISI_OFFLINE_HEPI_ON.index = 40
##########
DAMPED = get_idle_state(nodes, requestable=True)
DAMPED.index = 50

TURNING_ON_ST1_ISOLATION_LOOPS = get_move_control_loops_state(nodes, sub_names['ST1'], ISOLATED_STATE['ST1'])

ISOLATED_DAMPED = get_idle_state(nodes, requestable=True)
ISOLATED_DAMPED.index = 100

TURNING_ON_ST2_ISOLATION_LOOPS = get_move_control_loops_state(nodes, sub_names['ST2'], ISOLATED_STATE['ST2'])

FULLY_ISOLATED = get_idle_state(nodes, requestable=True)
FULLY_ISOLATED.index = 200

TURNING_OFF_ST2_BOOST = get_move_control_loops_state(nodes, sub_names['ST2'], 'HIGH_ISOLATED_NO_BOOST')
TURNING_ON_ST2_BOOST = get_move_control_loops_state(nodes, sub_names['ST2'], 'HIGH_ISOLATED')

FULLY_ISOLATED_NO_ST2_BOOST = get_idle_state(nodes, requestable=True)
FULLY_ISOLATED_NO_ST2_BOOST.index = 180

TURNING_OFF_ST2_ISOLATION_LOOPS = get_move_control_loops_state(nodes, sub_names['ST2'], 'DAMPED')
TURNING_OFF_ST1_ISOLATION_LOOPS = get_move_control_loops_state(nodes, sub_names['ST1'], 'DAMPED')

TURNING_OFF_HPI_ISOLATION_LOOPS = get_move_control_loops_state(nodes, sub_names['HPI'], 'READY')
TURNING_OFF_ST2_DAMPING_LOOPS = get_move_control_loops_state(nodes, sub_names['ST2'], 'READY')
TURNING_OFF_ST1_DAMPING_LOOPS = get_move_control_loops_state(nodes, sub_names['ST1'], 'READY')
